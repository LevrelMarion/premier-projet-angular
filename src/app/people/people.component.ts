import {Component, Input, OnInit} from '@angular/core';
import {People} from '../model/People';
import {StarwarsService} from '../service/starwars.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  @Input() people: People;

  constructor(
    private swService: StarwarsService
  ) {
    this.swService.getPeoples().subscribe(people => {
      console.log(people);
    });
  }

  ngOnInit() {
  }
  }

