import {Component, Input, OnInit} from '@angular/core';
import {Question} from '../model/question';

@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.css']
})
export class ListQuestionComponent implements OnInit {
  // création d'une liste questions;
  listeDeQuestions: Question[] = [];

  showQuestion(question: Question) {
    // return question !== this.listeDeQuestions[1];
    return true;
  }

  constructor() {
    this.listeDeQuestions.push(
      new Question(
        'Aimes-tu Star Wars ?',
        'Mouais',
        'peut-être'));
    this.listeDeQuestions.push(
      new Question(
        'Connais-tu Han Solo ?',
        'eventuellement',
        'bof'));
    this.listeDeQuestions.push(
      new Question(
        'As-tu vu tous les films Indiana Jones ?',
        'Si si',
        'Non non '));
  }

  ngOnInit() {
  }

}
