import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {People} from '../model/People';
import {Observable} from 'rxjs';
import {ListPeople} from '../model/ListPeople';

@Injectable()
export class StarwarsService {

  constructor(
    private http: HttpClient
  ) {
  }

 // getPeople(): Observable<People> {
  //  return this.http.get<People>('https://swapi.co/api/people/5');
 // }

  getPeoples(): Observable<ListPeople> {
    return this.http.get<ListPeople>('https://swapi.co/api/people');
  }
}
