import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListQuestionComponent } from './list-question/list-question.component';
import { QuestionComponent } from './question/question.component';
import { PeopleComponent } from './people/people.component';
import { ListPeopleComponent } from './list-people/list-people.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {StarwarsService} from './service/starwars.service';

const appRoutes: Routes = [
  { path: 'questions', component: ListQuestionComponent },
  { path: 'peoples', component: ListPeopleComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ListQuestionComponent,
    QuestionComponent,
    PeopleComponent,
    ListPeopleComponent,
      ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    ),
    HttpClientModule,
  ],
  providers: [
    StarwarsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
