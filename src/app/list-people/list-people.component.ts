import { Component, OnInit } from '@angular/core';
import {People} from '../model/People';
import {StarwarsService} from '../service/starwars.service';

@Component({
  selector: 'app-list-people',
  templateUrl: './list-people.component.html',
  styleUrls: ['./list-people.component.css']
})
export class ListPeopleComponent implements OnInit {
  listePeopleSW: People[] = [];
  load = true;

  constructor (private starwarsService: StarwarsService) {
    this.starwarsService.getPeoples().subscribe(listPeoples => {
      this.listePeopleSW = listPeoples.results;
      this.load = false;
    });
  }

    ngOnInit() {


      /*fetch('https://swapi.co/api/people')
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          const resultPeople = json.results;
          resultPeople.forEach(peopleSW => {
            this.listePeopleSW.push(peopleSW);
         });
        });*/
    }
  }
