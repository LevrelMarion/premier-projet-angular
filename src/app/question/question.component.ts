import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  @Input() enonce = 'question par défault';
// Input autorise à passer la variable à une balise app-question
  // si il n'y a rien dans enonce, (quand on fait le ForEach, alors il revient à question par défaut

  @Input() reponse1: string;
  @Input() reponse2: string;
  afficheReponse = false;
  textAnswer: string;

  constructor() {
  }

  ngOnInit() {
  }

  oneClick(reponse: string) {
    this.afficheReponse = true;
    this.textAnswer = reponse;
  }

}
